import email
import email.policy
from email.parser import BytesParser
from email.utils import parseaddr
from pathlib import Path
from typing import List

from lxml.html.clean import Cleaner
from lxml.html.soupparser import fromstring
from lxml.html import tostring


# This is just an in-memory dict of {message_id -> email.message.EmailMessage}.
# This can be replaced with a database/Model later.
messages_cache = {}


HTML_TEMPLATE = """
<HTML>
  <HEAD>
    <META http-equiv="Content-Type" content="text/html; charset=utf-8">
  </HEAD>
  <BODY>
    {html_body}
  </BODY>
</HTML>
"""

cleaner_retaining_img = Cleaner()
cleaner_retaining_img.javascript = True
cleaner_retaining_img.scripts = True
cleaner_retaining_img.style = True
cleaner_retaining_img.comments = True

cleaner_removing_img = Cleaner()
cleaner_removing_img.javascript = True
cleaner_removing_img.scripts = True
cleaner_removing_img.style = True
cleaner_removing_img.comments = True
cleaner_removing_img.kill_tags = ['img']


def _sanitize_html(html_or_plaintext: str, retain_img: bool) -> str:
    lxml_tree = fromstring(html_or_plaintext)
    if retain_img:
        clean_tree = cleaner_retaining_img.clean_html(lxml_tree)
    else:
        clean_tree = cleaner_removing_img.clean_html(lxml_tree)

    return tostring(clean_tree, encoding='unicode')


def load_messages_in_memory(maildir_path: str) -> None:
    # Loads emails from `maildir_path` to `messages_cache`
    maildir = Path(maildir_path)
    email_parser = BytesParser(policy=email.policy.default)
    for email_filename in maildir.glob('*.eml.txt'):
        email_path = maildir / email_filename
        with open(email_path, 'rb') as fp:
            message = email_parser.parse(fp)
            messages_cache[message['Message-ID']] = message


def get_message(message_id: str, retain_img: bool = False) -> dict:
    message = messages_cache[message_id]

    html_body = message.get_body(('html',))
    if not html_body:
        plain_body = message.get_body(('plain',))
        html_or_plaintext = plain_body.get_content() if plain_body else ''
    else:
        html_or_plaintext = html_body.get_content()
    html = _sanitize_html(
        html_or_plaintext,
        retain_img=retain_img
    )       # Would also do basic html rendering for plaintext
    from_name, from_address = parseaddr(message['From'])
    to_list = list(set(message.get_all('Delivered-To')))      # Deduplicate
    subject = message.get('Subject', '<no subject>')
    raw_headers = {key: value for key, value in message.items()}

    return {
        'message-id': message_id,
        'from': {
            'name': from_name,
            'address': from_address,
        },
        'to': to_list,
        'subject': subject,
        'raw-headers': raw_headers,
        'html-body': html
    }


def get_message_ids() -> List[str]:
    return list(messages_cache.keys())
