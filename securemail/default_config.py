from pathlib import Path


# maildir containing emails
MAILDIR = str(Path(__file__).parent.parent / 'maildir')

# host and port; used for running a local web server
BIND_HOST = '0.0.0.0'
BIND_PORT = 8000

# Flask & Flask-RESTful configs
ERROR_404_HELP = False
