from flask_restful import abort, reqparse, Api, Resource

from securemail.parser import get_message, get_message_ids


class Message(Resource):

    parser = reqparse.RequestParser()
    # Query string argument to include img tags in html-body of the response
    parser.add_argument('retain_img', type=bool, location='args')

    def get(self, message_id):
        args = self.parser.parse_args()
        retain_img = args.get('retain_img', False)
        try:
            return get_message(message_id, retain_img=retain_img)
        except KeyError:
            abort(404, message="message_id '{}' doesn't exist")


class MessageList(Resource):

    def get(self):
        return get_message_ids()


def add_email_api(app):
    api = Api(app)
    api.add_resource(MessageList, '/messages/')
    api.add_resource(Message, '/messages/<message_id>/')
