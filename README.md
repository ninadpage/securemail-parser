## A very simple REST API for a parser over a maildir

The API is implemented with Flask and
[Flask-RESTful](https://flask-restful.readthedocs.io/en/latest/index.html).


#### Requirements

We use [pip-tools](https://github.com/jazzband/pip-tools) to manage requirements. There is
one set of requirements - `requirements.in`, compiled to `requirements.txt`.


#### Setup (using virtualenv)

**Prerequisites**:

- Python 3 (tested with 3.6)

- [virtualenv](http://docs.python-guide.org/en/latest/dev/virtualenvs/)

**Steps**:

1. `git clone https://gitlab.com/ninadpage/securemail-parser`

2. `virtualenv .venv -p python3 && source .venv/bin/activate`

3. `pip install -r requirements.txt`

4. `python application.py` to start the application at `http://127.0.0.1:8000/` on your host -
   using Werkzeug (debug) server.


#### Config

We use [Flask-AppConfig](https://github.com/mbr/flask-appconfig) to configures the Flask
application in a canonical way.

**Supported config variables**:

* `MAILDIR`: The directory where email files are stored. Defaults to `<PROJECT_ROOT>/maildir`.
  Export environment variable `SECUREMAIL_MAILDIR` to override this. 

* `BIND_HOST` & `BIND_PORT`: Host & port for running the app using debug server.


#### API

* `GET /messages/`

  Returns a list of all emails' `Message-ID`s. Ideally, this should also include few other fields
  (such as subject, sender name, received date, etc) for a Frontend client to display useful data
  list of emails in the inbox.
  
* `GET /messages/<message_id>/`

  Returns full details of a message. HTML body is sanitized (all `script`, `style` and `img` tags
  are stripped). Supports optional query parameter `retain_img` which will retain `img` in HTML
  body (e.g. `GET /messages/<message_id>/?retain_img=true`).


#### Limitations & Future Improvements

* Tests!
* Persistence: Currently the maildir is loaded in an in-memory cache at init time,
  which means any new files in maildir aren't visible until the app is restarted.
  For any real-world usage, this would need a persistent storage (database), with
  ideally storing pre-parsed emails.
* Better support for `multipart` (attachments, etc).
* For stripping only external resources during HTML sanitization, `host_whitelist`
  of `lxml.html.clean.Cleaner` can be used.
