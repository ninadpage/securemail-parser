import logging

from flask import Flask
from flask_appconfig import AppConfig

from securemail.api import add_email_api
from securemail.parser import load_messages_in_memory


def create_app(configfile=None):
    app = Flask('securemail')
    AppConfig(app, configfile)
    return app


# Initialize Flask app
application = create_app()
add_email_api(application)

# Initialize in-memory cache of messages
# WARNING: Since messages are loaded only during the app init, new messages
# placed in maildir will not be visible until the app is restarted
load_messages_in_memory(application.config['MAILDIR'])

# Configure logging
log_level = logging.DEBUG if application.debug else logging.INFO
logging.basicConfig(level=log_level,
                    format='[%(asctime)s %(levelname)-8s] [%(name)s] %(message)s')


if __name__ == '__main__':
    application.run(host=application.config['BIND_HOST'], port=application.config['BIND_PORT'])
